<div id="main">
<div class="title">Overview</div>
<div class="content">
<p>
MX4J is a project to build an Open Source implementation of the Java(TM) Management Extensions
(JMX) and of the JMX Remote API (JSR 160) specifications, and to build tools relating to JMX.
</p>
<p>JMX is an optional package for J2SE that provides a standard way to manage applications.
It can also be used to wrap legacy systems and provide a standard interface to the outside world,
enabling the development of web services. JMX allows developers to write more modular and loosely
coupled system components and reduce the complexity of large, interoperating systems.
</p>
<p>We have a lot more to say about how this is done. <br/>
You can learn more about JMX from the MX4J documentation and from the JMX home page.
</p>
<p>The MX4J project has these primary goals:</p>
<ul>
<li>Closely adhere to the JMX specification.</li>
<li>Develop a highly robust codebase.</li>
<li>Be 100% compatible with the reference implementations.</li>
</ul>
<p>We run more than 1000 <a href="http://junit.org">JUnit</a> tests every night to accomplish these goals.
Unit testing helps us (and you) to gauge the stability and growth of our project.
</p>
<?php include 'news.php';?>
</div>
</div>
