<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
   <title>MX4J - Open Source Java Management Extensions</title>
   <link rel="stylesheet" href="stylesheet.css">
   <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
   <meta name="description" content="MX4J, an open source implementation for the JMX(TM) technology, version 1.1">
   <meta name="keywords" content="JMX,implementation,JMX implementation,JSR 3,JSR 160,JMX Remote API,1.2,specification,MX4J,javax.management,server-side,management,application,connector,adaptor,MBeans,MBean,ModelMBean,RequiredModelMBean,managed,bean,beans,Agent,instrumentation">
</head>

<div>
<table cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td><img src="images/logo.gif" alt=""/></td>
      <td><img src="images/slogan.gif" alt=""/></td>
   </tr>
</table>
</div>
