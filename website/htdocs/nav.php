<div id="left">
<div id="nav">

<div class="title">Community</div>
<div class="content">
<a href="/">Home</a>
<br />
<a href="http://sourceforge.net/projects/mx4j">Development</a>
<br />
<a href="http://sourceforge.net/mail/?group_id=47745">Mailing Lists</a>
</div>

<div class="title">Documentation</div>
<div class="content">
<a href="/docs/index.html">MX4J Guide</a>
<br />
<a href="/docs/api/index.html">MX4J API Documentation</a>
<br />
<a href="http://java.sun.com/jmx">JMX Home Page</a>
</div>

<div class="title">Daily Test Reports</div>
<div class="content">
<!--
<a href="/test/1.3.1/index.html">JDK 1.3.1</a>
<br />
-->
<a href="/test/1.4.2/index.html">JDK 1.4.2</a>
</div>

<div class="title">Download</div>
<div class="content">
<a href="http://sourceforge.net/project/showfiles.php?group_id=47745">Latest Release</a><br/>
<a href="/mx4j.zip">Nightly CVS code</a>
</div>

<div class="title">Projects using MX4J</div>
<div class="content">
<a href="http://jetty.mortbay.com" target="out">Jetty</a><br>
<a href="http://jakarta.apache.org/tomcat" target="out">Apache's Tomcat</a><br>
<a href="http://jonas.objectweb.org" target="out">ObjectWeb's JOnAS</a><br/>
<a href="http://geronimo.apache.org" target="out">Apache's Geronimo</a><br/>
<a href="http://jakarta.apache.org/avalon/phoenix/index.html" target="out">Avalon-Phoenix 4.0</a><br/>
<a href="http://kalixia.com/projects/WebOS/" target="out">WebOS</a><br/>
<a href="http://mc4j.sf.net" target="out">MC4J</a><br/>
<a href="http://www.jpos.org" target="out">jPOS</a><br/>
and many others...
</div>
</div>

<div class="iconLink">
<a href="http://sourceforge.net">
<img src="http://sourceforge.net/sflogo.php?group_id=47745&amp;type=1" width="88" height="31" border="0" alt="SourceForge.net Logo" />
</a>
</div>

<div class="iconLink">
<a href="http://validator.w3.org/check/referer">
<img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01!" />
</a>
</div>

<div class="iconLink">
<a href="http://jigsaw.w3.org/css-validator/check/referer">
<img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
</a>
</div>

<div class="iconLink">
<a target="_top" href="http://v.extreme-dm.com/?login=openjmx">
<img name="im" src="http://v1.extreme-dm.com/i.gif" height="38" border="0" width="41" alt=""/>
</a>
<script type="text/javascript"><!--
an=navigator.appName;d=document;function
pr(){d.write("<img src=\"http://v0.extreme-dm.com",
"/0.gif?tag=openjmx&j=y&srw="+srw+"&srb="+srb+"&",
"rs="+r+"&l="+escape(d.referrer)+"\" height=1 ",
"width=1>");}srb="na";srw="na";//-->
</script><script type="text/javascript1.2"><!--
s=screen;srw=s.width;an!="Netscape"?
srb=s.colorDepth:srb=s.pixelDepth;//-->
</script><script type="text/javascript"><!--
r=41;d.images?r=d.im.width:z=0;pr();//-->
</script><noscript><img height="1" width="1" alt=""
src="http://v0.extreme-dm.com/0.gif?tag=openjmx&amp;j=n"></noscript>
</div>

</div>
