<div class="title">News</div>
<div class="content">
<p>
MX4J 3.0.1 has been released, a bug fix release over 3.0.0 since few
bugs slipped in (release 3.0.1 restores JDK 1.3.x compatibility that
was broken in 3.0.0).
</p>
<p>
MX4J 3.0.x brings a lot of bug fixes and the possibility to run JSR 160
HTTP-based connectors over the SSL protocol with a simpler syntax, for
example:
<pre>service:jmx:soap+ssl://host:8080/soap</pre>
Here's the link to download
<a href="http://sourceforge.net/project/showfiles.php?group_id=47745">MX4J 3.0.1</a>
</p>
<span class="poster">Posted by simonebordet, 1 Mar 2005</span>
<p />
</div>
<div class="content">
<p>
MX4J 2.1.1 has been released.
<br />
MX4J is working with the <a href="http://geronimo.apache.org">Geronimo</a>
project pass the J2EE compliance suite.
</p>
<span class="poster">Posted by simonebordet, 8 Feb 2005</span>
</div>
<div class="content">
<p>
The Hessian and Burlap connectors are completed. <br/>
Fixing bugs, working on JSR 160 security, and making MX4J compliant to allow
application servers such as <a href="http://jonas.objectweb.org">JOnAS</a>
and <a href="http://geronimo.apache.org">Geronimo</a> pass the J2EE 1.4
compliance suite.
</p>
<span class="poster">Posted by biorn_steedom, 4 Sep 2004</span>
</div>
<div class="content">
<p>
A new effort has been started to implement 2 more providers for JSR 160.<br/>
The 2 new protocols will be <strong>hessian</strong> and <strong>burlap</strong>
from <a href="http://www.caucho.com">Caucho</a>.
Join the MX4J developer mailing list and drop a message if you would like to contribute.
</p>
<span class="poster">Posted by biorn_steedom, 20 Jul 2004</span>
</div>
<div class="content">
<p>
Working on the SOAPConnectorServer, making it simpler to use and fixed some bug.<br/>
The improved version is now committed in CVS, and documentation is coming.
</p>
<span class="poster">Posted by biorn_steedom, 19 Jul 2004</span>
</div>
<div class="content">
<p>
<a href="http://sourceforge.net/project/showfiles.php?group_id=47745">MX4J 2.0.1</a> has been released ! <br/>
A bug fix release that fixes problems with MBeanDescriptions, RMI marshalling and ModelMBeans.
</p>
<span class="poster">Posted by biorn_steedom, 2 Feb 2004</span>
</div>
<div class="content">
<p>
MX4J 2.0 has been released ! <br/>
MX4J 2.0 implements the JMX 1.2.1 and JSR 160 1.0 specifications, and offers many tools
to ease and speed up JMX application development. <br />
Check it out <a href="http://sourceforge.net/project/showfiles.php?group_id=47745">here</a>.
</p>
<span class="poster">Posted by biorn_steedom, 2 Feb 2004</span>
</div>
<div class="content">
<p>
Working on performance and memory footprint for standard MBeans: huge improvements ! <br />
Some more bug fix and then we're ready for final.
</p>
<span class="poster">Posted by biorn_steedom, 2 Feb 2004</span>
</div>
<div class="content">
<p>
MX4J 2.0 beta 1 is finally out ! <br/>
Check it out <a href="http://sourceforge.net/project/showfiles.php?group_id=47745">here</a>.
</p>
<span class="poster">Posted by biorn_steedom, 26 Nov 2003</span>
</div>
<div class="content">
<p>
JMX 1.2.1 and JSR 160 implementation is complete.
MX4J 2.0 Beta 1 release is due next week.
</p>
<span class="poster">Posted by biorn_steedom, 22 Nov 2003</span>
</div>
<div class="content">
<p>
Web site revamped ! <br/>
Thanks to n_alex_rupp and his ability, MX4J has a new web site.
</p>
<span class="poster">Posted by biorn_steedom, 22 Nov 2003</span>
</div>
