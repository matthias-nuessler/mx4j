@echo off

setlocal

if not "%JAVA_HOME%"=="" goto start

echo JAVA_HOME not set
goto end

:start

set JDK=%JAVA_HOME%

set CP=ant.jar
set CP=%CP%;ant-launcher.jar
set CP=%CP%;ant-junit.jar
set CP=%CP%;ant-trax.jar
set CP=%CP%;..\lib\xercesImpl.jar
set CP=%CP%;..\lib\xml-apis.jar
set CP=%CP%;..\lib\xalan.jar
set CP=%CP%;..\lib\junit.jar
set CP=%CP%;%JDK%\lib\tools.jar

%JDK%\bin\java -cp "%CP%" org.apache.tools.ant.Main %*

:end
