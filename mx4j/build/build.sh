#!/bin/sh

if test -z "${JAVA_HOME}" ; then
    echo "ERROR: JAVA_HOME not found in your environment."
    exit
fi

cygwin=false
case "`uname`" in
CYGWIN*) cygwin=true;;
esac

export CLASSPATH=ant.jar
export CLASSPATH=$CLASSPATH:ant-launcher.jar
export CLASSPATH=$CLASSPATH:ant-junit.jar
export CLASSPATH=$CLASSPATH:ant-trax.jar
export CLASSPATH=$CLASSPATH:../lib/xercesImpl.jar
export CLASSPATH=$CLASSPATH:../lib/xml-apis.jar
export CLASSPATH=$CLASSPATH:../lib/xalan.jar
export CLASSPATH=$CLASSPATH:../lib/junit.jar
export CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/tools.jar

if $cygwin; then
        [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --windows "$JAVA_HOME"`
        [ -n "$CLASSPATH" ] && CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi

$JAVA_HOME/bin/java -classpath $CLASSPATH org.apache.tools.ant.Main $@
