/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package test.mx4j;

import junit.framework.TestCase;

/**
 * Base class for MX4J tests
 *
 * @version $Revision$
 */
public abstract class MX4JTestCase extends TestCase
{
    protected void sleep(long time)
    {
        try
        {
            Thread.sleep(time);
        }
        catch (InterruptedException x)
        {
            Thread.currentThread().interrupt();
        }
    }

    protected String getCurrentProjectBaseDir()
    {
        return System.getProperty("basedir");
    }

    protected String getFileSeparator()
    {
        return System.getProperty("file.separator");
    }

    protected String getCurrentTestDir()
    {
        return getCurrentProjectBaseDir()+getFileSeparator()+"target"+getFileSeparator()+"test-classes/";
    }

    protected String getJSR003TestDir()
    {
        return getCurrentProjectBaseDir()+getFileSeparator()+".."+getFileSeparator()+"jsr003"+getFileSeparator()+"target"+getFileSeparator()+"test-classes/";
    }

    protected String getJSR160TestDir()
    {
        return getCurrentProjectBaseDir()+getFileSeparator()+".."+getFileSeparator()+"jsr160"+getFileSeparator()+"target"+getFileSeparator()+"test-classes/";
    }

    protected String getJMXRIBasePath()
    {
    	return getCurrentProjectBaseDir()+getFileSeparator()+".."+getFileSeparator()+"jmxri"
    		+getFileSeparator()+"lib"+getFileSeparator();
    }
    
    protected String getJMXRIJarPath()
    {
        return getJMXRIBasePath()+"jmxri.jar";
    }

    protected String getJMXRIRemoteJarPath()
    {
        return getJMXRIBasePath()+"jmxremote.jar";
    }

    protected String getJMXRIRemoteOptionalJarPath()
    {
        return getJMXRIBasePath()+"jmxremote_optional.jar";
    }

    protected String getJAASJarPath()
    {
        return getJMXRIBasePath()+"jaas-1.0.01.jar";
    }
}
