package test.mx4j;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @version $Revision$ $Date$
 */
public abstract class MX4JClassLoadingTestCase extends MX4JTestCase
{
    protected ClassLoader createMX4JClassLoader() throws MalformedURLException
    {
        return getClass().getClassLoader();
    }

    protected ClassLoader createJMXRIClassLoader() throws MalformedURLException
    {
        File jmxri = new File(getJMXRIJarPath());
        if (!jmxri.exists()) fail("JMXRI jar is not available");
        return new URLClassLoader(new URL[]{jmxri.toURL()}, getClass().getClassLoader().getParent());
    }

    protected ClassLoader createJMXRIWithMX4JImplClassLoader() throws MalformedURLException
    {
        File jmxri = new File(getJMXRIJarPath());
        if (!jmxri.exists()) fail("JMXRI jar is not available");
        return new URLClassLoader(new URL[]{jmxri.toURL()}, getClass().getClassLoader());
    }

    protected ClassLoader createMX4JWithTestsClassLoader() throws MalformedURLException
    {
        return getClass().getClassLoader();
    }

    protected ClassLoader createRemoteMX4JWithTestsClassLoader() throws MalformedURLException
    {
        File jaas = new File(getJAASJarPath());
        return new URLClassLoader(new URL[]{jaas.toURL()}, getClass().getClassLoader());
    }

    protected ClassLoader createJMXRIWithTestsClassLoader() throws MalformedURLException
    {
        File jmxri = new File(getJMXRIJarPath());
        if (!jmxri.exists()) fail("JMXRI jar is not available");
        File tests003 = new File(getJSR003TestDir());
        File tests160 = new File(getJSR160TestDir());
        return new URLClassLoader(new URL[]{jmxri.toURL(), tests003.toURL(), tests160.toURL()}, getClass().getClassLoader().getParent());
    }

    protected ClassLoader createRemoteJMXRIWithTestsClassLoader() throws MalformedURLException
    {
        File jmx = new File(getJMXRIJarPath());
        if (!jmx.exists()) fail("JMXRI jar is not available");
        File rjmx = new File(getJMXRIRemoteJarPath());
        if (!rjmx.exists()) fail("JMX Remote jar is not available");
        File jaas = new File(getJAASJarPath());
        File tests003 = new File(getJSR003TestDir());
        File tests160 = new File(getJSR160TestDir());
        return new URLClassLoader(new URL[]{jaas.toURL(), jmx.toURL(), rjmx.toURL(), tests003.toURL(), tests160.toURL()}, getClass().getClassLoader().getParent());
    }

    protected ClassLoader createOptionalRemoteJMXRIWithTestsClassLoader() throws MalformedURLException
    {
        File jmx = new File(getJMXRIJarPath());
        if (!jmx.exists()) fail("JMXRI jar is not available");
        File rjmx = new File(getJMXRIRemoteJarPath());
        if (!rjmx.exists()) fail("JMX Remote jar is not available");
        File orjmx = new File(getJMXRIRemoteOptionalJarPath());
        if (!orjmx.exists()) fail("JMX Optional Remote jar is not available");
        File tests003 = new File(getJSR003TestDir());
        File tests160 = new File(getJSR160TestDir());
        return new URLClassLoader(new URL[]{jmx.toURL(), rjmx.toURL(), orjmx.toURL(), tests003.toURL(), tests160.toURL()}, getClass().getClassLoader().getParent());
    }
}
