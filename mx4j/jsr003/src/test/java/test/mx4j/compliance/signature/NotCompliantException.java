/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package test.mx4j.compliance.signature;

/**
 * @version $Revision:2260 $
 */
public class NotCompliantException extends Exception
{
   public NotCompliantException(String s)
   {
      super(s);
   }
}
