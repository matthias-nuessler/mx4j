/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package test.mx4j.compliance.signature;

import java.lang.reflect.Field;

/**
 * @version $Revision:2260 $
 */
public class FieldWrapper extends MemberWrapper
{
   public FieldWrapper(Field field)
   {
      modifiers = field.getModifiers();
      type = field.getType().getName();
      name = field.getName();
   }
}
