/*
 * � Copyright 2004 Hewlett-Packard
 */
package test.mx4j.server;

import java.util.List;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanRegistrationException;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ReflectionException;

import mx4j.server.MBeanMetaData;
import mx4j.server.interceptor.MBeanServerInterceptor;

/**
 * $Rev$
 */
public class NoOpMBeanServerInterceptor implements MBeanServerInterceptor
{
   public String getType()
   {
      return "noop";
   }

   public void setChain(List interceptors)
   {
   }

   public void addNotificationListener(MBeanMetaData metadata, NotificationListener listener, NotificationFilter filter, Object handback)
   {
   }

   public void removeNotificationListener(MBeanMetaData metadata, NotificationListener listener) throws ListenerNotFoundException
   {
   }

   public void removeNotificationListener(MBeanMetaData metadata, NotificationListener listener, NotificationFilter filter, Object handback) throws ListenerNotFoundException
   {
   }

   public void instantiate(MBeanMetaData metadata, String className, String[] params, Object[] args) throws ReflectionException, MBeanException
   {
   }

   public void registration(MBeanMetaData metadata, int operation) throws MBeanRegistrationException
   {
   }

   public MBeanInfo getMBeanInfo(MBeanMetaData metadata)
   {
      return null;
   }

   public Object invoke(MBeanMetaData metadata, String method, String[] params, Object[] args) throws MBeanException, ReflectionException
   {
      return null;
   }

   public AttributeList getAttributes(MBeanMetaData metadata, String[] attributes)
   {
      return null;
   }

   public AttributeList setAttributes(MBeanMetaData metadata, AttributeList attributes)
   {
      return null;
   }

   public Object getAttribute(MBeanMetaData metadata, String attribute) throws MBeanException, AttributeNotFoundException, ReflectionException
   {
      return null;
   }

   public void setAttribute(MBeanMetaData metadata, Attribute attribute) throws MBeanException, AttributeNotFoundException, InvalidAttributeValueException, ReflectionException
   {
   }
}
