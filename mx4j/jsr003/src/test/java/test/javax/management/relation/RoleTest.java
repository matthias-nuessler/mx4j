/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package test.javax.management.relation;

import java.util.ArrayList;
import java.util.List;

import javax.management.ObjectName;
import javax.management.relation.Role;

import test.mx4j.MX4JTestCase;

/**
 * @version $Revision:2260 $
 */
public class RoleTest extends MX4JTestCase
{
    private Role _role;

    protected void setUp() throws Exception
    {
        _role = new Role("test Role", new ArrayList());
    }

    protected void tearDown() throws Exception
    {
        _role = null;
    }

    public void testGetRoleValue() throws Exception
    {
        List roleValues = _role.getRoleValue();
        assertNotNull(roleValues);
    }

    public void testGetRoleValue_ListHasElements() throws Exception
    {
        List values = new ArrayList();
        values.add(new ObjectName("domain:name=test"));
        Role role = new Role("Test Role", values);
        assertEquals(1, role.getRoleValue().size());
    }
}