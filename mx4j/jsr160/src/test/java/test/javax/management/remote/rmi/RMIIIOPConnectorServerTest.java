/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package test.javax.management.remote.rmi;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

import org.omg.CORBA.ORB;

/**
 * @version $Revision$
 */
public class RMIIIOPConnectorServerTest extends RMIConnectorServerTestCase
{
//   private CosNamingService naming;

   public void startNaming() throws Exception
   {
//      naming = new CosNamingService(getNamingPort());
//      naming.start();
      Thread.sleep(5000);
   }

   public void stopNaming() throws Exception
   {
//      naming.stop();
//      naming = null;
      Thread.sleep(5000);
   }

   public int getNamingPort()
   {
      return 1100;
   }

   public JMXServiceURL createJMXConnectorServerAddress() throws MalformedURLException
   {
      return new JMXServiceURL("iiop", "localhost", 0);
   }

   public Map getEnvironment()
   {
      HashMap env = new HashMap();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.cosnaming.CNCtxFactory");
      env.put(Context.PROVIDER_URL, "iiop://localhost:" + getNamingPort());
      return env;
   }

   public void testInvalidProvidedORB() throws Exception
   {
      Map environment = new HashMap();
      environment.put("java.naming.corba.orb", new Object());

      JMXServiceURL url = new JMXServiceURL("iiop", null, 0, "/jndi/iiop://localhost:" + getNamingPort() + "/jmx");
      try
      {
         JMXConnectorServer connectorServer = JMXConnectorServerFactory.newJMXConnectorServer(url, environment, newMBeanServer());
         connectorServer.start();
         fail();
      }
      catch (IllegalArgumentException ignored)
      {
      }
   }

   public void testProvidedORB() throws Exception
   {
      ORB orb = ORB.init((String[])null, null);
      Map environment = new HashMap();
      environment.put("java.naming.corba.orb", orb);

      JMXServiceURL url = new JMXServiceURL("iiop", null, 0, "/jndi/iiop://localhost:" + getNamingPort() + "/jmx");
      JMXConnectorServer connectorServer = null;
      try
      {
         startNaming();
         connectorServer = JMXConnectorServerFactory.newJMXConnectorServer(url, environment, newMBeanServer());
         connectorServer.start();
      }
      finally
      {
         if (connectorServer != null) connectorServer.stop();
         stopNaming();
      }
   }
}
