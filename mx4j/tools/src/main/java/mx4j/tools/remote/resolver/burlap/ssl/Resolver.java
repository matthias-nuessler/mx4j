/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package mx4j.tools.remote.resolver.burlap.ssl;

import java.util.Map;

import mx4j.tools.remote.caucho.burlap.SSLBurlapServlet;

/**
 * @version $Revision$
 */
public class Resolver extends mx4j.tools.remote.resolver.burlap.Resolver
{
   protected String getEndpointProtocol(Map environment)
   {
      return "https";
   }

   protected String getServletClassName()
   {
      return SSLBurlapServlet.class.getName();
   }
}
