/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package mx4j.tools.remote.soap.axis.ser;

import java.util.HashSet;
import java.util.Set;
import javax.security.auth.Subject;

import org.xml.sax.SAXException;

/**
 * @version $Revision$
 */
public class SubjectDeser extends AxisDeserializer
{
   private boolean readOnly;
   private Set principals;
   private Set publicCredentials;
   private Set privateCredentials;

   public void onSetChildValue(Object value, Object hint) throws SAXException
   {
      if (SubjectSer.READ_ONLY.equals(hint))
         readOnly = ((Boolean)value).booleanValue();
      else if (SubjectSer.PRINCIPALS.equals(hint))
         principals = getValueAsSet(value);
      else if (SubjectSer.PUBLIC_CREDENTIALS.equals(hint))
         publicCredentials = getValueAsSet(value);
      else if (SubjectSer.PRIVATE_CREDENTIALS.equals(hint)) 
    	 privateCredentials = getValueAsSet(value);
   }

   /**
	 * Current implementation assumes two data types: an array of some type of
	 * Objects OR some object that implements Set
	 * 
	 * @param value
	 *            the value to convert into a set
	 * @return either the value cast as a Set OR an instance of a Set that the
	 *         values of the array were copied into.
	 */
   private Set getValueAsSet(Object value) 
   {
	   Set result = null;
	   if (value != null && value.getClass().isArray()) {
		   final Object[] valueArr = (Object[])value;
		   result = new HashSet(valueArr.length);
		   for (int i=0; i<valueArr.length; i++) {
			   result.add(valueArr[i]);
		   }
	   } else {
		   result = (Set)value;
	   }
	   return result;
   }
   
   protected Object createObject() throws SAXException
   {
      return new Subject(readOnly, principals, publicCredentials, privateCredentials);
   }
}
