/*
 * Copyright (C) The MX4J Contributors.
 * All rights reserved.
 *
 * This software is distributed under the terms of the MX4J License version 1.0.
 * See the terms of the MX4J License in the documentation provided with this software.
 */

package mx4j.tools.remote.soap;

/**
 * @version $Revision$
 */
class SOAPConstants
{
   static final String NAMESPACE_URI = "http://mx4j.sourceforge.net/remote/soap/1.0";

   static final String CONNECTION_ID_HEADER_NAME = "connectionId";

   private SOAPConstants()
   {
   }
}
