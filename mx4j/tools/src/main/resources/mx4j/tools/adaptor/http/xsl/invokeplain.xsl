<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="text" />

   <!-- Overall parameters -->
   <xsl:param name="html.stylesheet">stylesheet.css</xsl:param>
   <xsl:param name="html.stylesheet.type">text/css</xsl:param>
   <xsl:param name="head.title">invoke.title</xsl:param>

   <!-- Request parameters -->
   <xsl:param name="request.objectname"/>
   <xsl:param name="request.method"/>

   <xsl:include href="common.xsl"/>
   <xsl:include href="mbean_attributes.xsl"/>

   <!-- Operation invoke -->
   <xsl:template name="operation">
      <xsl:for-each select="Operation">
         <xsl:if test="@result='success'">
            <xsl:if test="not (@return='')">
               <xsl:call-template name="str">
                  <xsl:with-param name="id">invoke.operation.success.resultplain</xsl:with-param>
                  <xsl:with-param name="p0">
                     <xsl:call-template name="renderobjectescapeoption">
                        <xsl:with-param name="objectclass" select="@returnclass"/>
                        <xsl:with-param name="objectvalue" select="@return"/>
                        <xsl:with-param name="objectvalueunescaped" select="@returnunescaped" />
                     </xsl:call-template>
                  </xsl:with-param>
               </xsl:call-template>
            </xsl:if>
            <xsl:if test="@return=''">
               <xsl:call-template name="str">
                  <xsl:with-param name="id">invoke.operation.success.noresult</xsl:with-param>
               </xsl:call-template>
            </xsl:if>
         </xsl:if>
         <xsl:if test="@result='error'">
            <xsl:call-template name="str">
               <xsl:with-param name="id">invoke.operation.success.error</xsl:with-param>
               <xsl:with-param name="p0">
                  <xsl:value-of select="@errorMsg"/>
               </xsl:with-param>
            </xsl:call-template>
         </xsl:if>
      </xsl:for-each>
   </xsl:template>
   
   <!-- Main template -->
   <xsl:template match="MBeanOperation" name="main">
     <xsl:call-template name="operation"/>
   </xsl:template>
</xsl:stylesheet>
   
