<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <xsl:import href="../docbookx/html/chunk.xsl"/>
  <xsl:import href="autotoc.xsl"/>

  <xsl:param name="html.stylesheet">styles.css</xsl:param>

  <xsl:param name="toc.book.depth">0</xsl:param>

</xsl:stylesheet>
